# Date: 2023-05-31
# Kernel: Linux 5.1.19-v8+
# Processor: Raspberry Pi CM4 / Broadcom BCM2711 (1.5GHz, not overclocked)

pi@raspberrypi:~ $ 7z b

7-Zip [32] 16.02 : Copyright (c) 1999-2016 Igor Pavlov : 2016-05-21
p7zip Version 16.02 (locale=en_US.UTF-8,Utf16=on,HugeFiles=on,32 bits,4 CPUs LE)

LE
CPU Freq: 12800000 16000000 32000000 32000000 64000000 128000000 256000000 512000000 1024000000

RAM size:    7636 MB,  # CPU hardware threads:   4
RAM usage:    882 MB,  # Benchmark threads:      4

                       Compressing  |                  Decompressing
Dict     Speed Usage    R/U Rating  |      Speed Usage    R/U Rating
         KiB/s     %   MIPS   MIPS  |      KiB/s     %   MIPS   MIPS

22:       4177   370   1097   4064  |     114492   391   2501   9768
23:       3884   370   1070   3958  |      89991   389   2001   7787
24:       3263   355    988   3509  |      63935   391   1437   5613
25:       2465   345    817   2815  |      47659   390   1088   4242
----------------------------------  | ------------------------------
Avr:             360    993   3586  |              390   1757   6852
Tot:             375   1375   5219
